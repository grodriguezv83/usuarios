console.log("Aquí funcionando con nodemon")

var movimientoJSON = require('./movimientosv2.json')
var movimientoUsuariosJSON = require('./usuarios.json')
var express = require('express')
var bodyparser = require('body-parser')
var app = express()

app.use(bodyparser.json())

app.get('/', function(req, res)
{
  res.send('Hola API')
})

app.get('/v1/movimientos', function(req,res)
{
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos', function(req,res)
{
  //res.sendfile('movimientosv2.json')
  res.send(movimientoJSON)
})

app.get('/v2/movimientos/:id', function(req,res)
{
  //console.log(req)
  console.log(req.params.id)
  //console.log (movimientoJSON)
  //res.send('Hemos recibido su petición de consulta del movimiento #' + req.params.id)
  res.send(movimientoJSON[req.params.id-1])
})

app.post('/v2/movimientos', function(req,res)
{
  console.log(req)
  //console.log(req.headers['authorization'])
  //if(req.headers['authorization']==undefined)
  //{
    var nuevo = req.body
    nuevo.id= movimientoJSON.length + 1
    movimientoJSON.push(nuevo)
    res.send("Movimiento dado de alta")
  //}else {
  //  res.send('No está autorizado')
  //}

})

app.get('/v2/movimientosq', function(req,res)
{
  console.log(req.query)
  res.send("recibido")
})

app.get('/v2/movimientosp', function(req,res)
{
  console.log(req.params)
  res.send("recibido")
})

app.put('/v2/movimientos/:id', function(req,res)
{
  var cambios = req.body
  var actual = movimientoJSON[req.params.id-1]

  if(cambios.importe != undefined)
  {
    actual.importe = cambios.importe
  }

  if(cambios.ciudad != undefined)
  {
    actual.ciudad = cambios.ciudad
  }
  res.send("Cambios realizados")
})

app.delete('/v2/movimientos/:id', function(req,res)
{
  var actual = movimientoJSON[req.params.id-1]
  movimientoJSON.push({
    "id" : movimientoJSON.length+1,
    "ciudad" : actual.ciudad,
    "importe" : actual.importe*(-1),
    "concepto" : "Negativo del " + req.params.id

  })

  res.send("Movimiento anulado")
})

//GET DE USUARIOS
app.get('/usuarios/:id', function(req,res)
{
  //console.log(req)
  console.log(req.params.id)
  //console.log (movimientoJSON)
  //res.send('Hemos recibido su petición de consulta del movimiento #' + req.params.id)
  res.send(movimientoUsuariosJSON[req.params.id-1])
})

//POST LOGIN DE usuarios
app.post('/usuarios/login', function(req,res)
{

  var entro = false;

  for(var i = 0; i < movimientoUsuariosJSON.length; i++){
    if(movimientoUsuariosJSON[i].email == req.headers['email'] && movimientoUsuariosJSON[i].password == req.headers['password']){
      entro = true;
      movimientoUsuariosJSON[i].estatus = true;
      console.log(movimientoUsuariosJSON[i])
    }
  }

  if(entro)
  {
    res.send("Usuario: " + req.headers['email'] + " entro")
  }else {
    res.send("Usuario: " + req.headers['email'] + " no tiene PERMISOS")
  }

})

//POST LOGOUT DE USUARIOS
//POST LOGIN DE usuarios
app.post('/usuarios/logout', function(req,res)
{
  var actual = movimientoUsuariosJSON[req.headers['id']-1]
console.log("Antes de entrar")
console.log(actual)

if(actual != undefined)
{
  if(actual.estatus == true)
  {
    actual.estatus = false;
    res.send("Usuario: " + req.headers['email'] + " salio")
  }else {
    res.send("Usuario: " + req.headers['email'] + " no habia entrado")
  }
console.log("Al salir")
  console.log(actual)

}else {
  res.send("Error al salir del usuaurio: " + req.headers['email'])
}


})


app.listen(3000)

console.log("Escuchando en el puerto 3000")
